from selenium import webdriver
import unittest
import HtmlTestRunner
from pages import MainPage, SupplyListPage, SupplyAddPage, LoginPage, MessagePage


class BaseTest(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.maximize_window()
        self.driver.get("http://core:8000/")

        self.main_page = MainPage(self.driver)
        self.supply_list_page = SupplyListPage(self.driver)
        self.supply_add_page = SupplyAddPage(self.driver)
        self.login_page = LoginPage(self.driver)
        self.message_page = MessagePage(self.driver)

    def tearDown(self):
        self.driver.quit()


class SupplyTest(BaseTest):

    def test_add_supply_past_date(self):
        self.login_page.login("admin", "123456")
        self.main_page.access_menu("Abastecimentos")
        self.supply_list_page.add_button()
        self.supply_add_page.add("18599", "162.32", "44", "11/08/2020", "14:22:00")

        self.assertTrue(self.message_page.success_visible())

    def test_add_supply_without_date(self):
        self.login_page.login("admin", "123456")
        self.main_page.access_menu("Abastecimentos")
        self.supply_list_page.add_button()
        self.supply_add_page.add("18599", "162.32", "44", "", "")

        self.assertTrue(self.message_page.required_visible())


if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='test_result'))
